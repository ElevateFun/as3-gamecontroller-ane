//
// IMPORTANT:
// to get ane's working, there are four steps
// 
// 1) Project Properties -> ActionScript Build Path ->
// Native Extensions -> Add ANE
//
// 2) in either step 1 or 3, let the ane definition be 
// added to the .xml file
//
// 3) Project Properties -> ActionScript Build Packaging ->
// Native Extensions -> (find the checkbox under the "package"
// column) Check Checkbox
//
// 4) use com.lanceulmer.OSXANEFixer (explanation in
// that file)
//

package
{
	import flash.display.Sprite;
	
	// import ca.wegetsignal.nativeextensions.MacJoystick
	// import ca.wegetsignal.nativeextensions.MacJoystickEvent
	import ca.wegetsignal.nativeextensions.MacJoystickManager
	
	import com.lanceulmer.OSXANEFixer;
	
	public class JoystickDemo extends Sprite
	{
		private var macJoystickManager:MacJoystickManager = null;
		
		public function JoystickDemo()
		{	
			function onFix(extensionID:String, success:Boolean):void {
				if (success) {
					macJoystickManager = new MacJoystickManager();
					trace("Number of joysticks:", macJoystickManager.numberOfJoysticks());
				}
			}
			
			// read the docs on why this is used
			OSXANEFixer.fixANE("ca.wegetsignal.nativeextensions.MacJoyANE", onFix);
			// to see what would happen without the fixer, use this line instead
			// onFix("ca.wegetsignal.nativeextensions.MacJoyANE", true);
		}
	}
}